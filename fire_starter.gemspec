$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "fire_starter/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "fire_starter"
  s.version     = FireStarter::VERSION
  s.authors     = ["Nico Durstewitz"]
  s.email       = ["nico.durstewitz@ressourcenmangel.de"]
  s.homepage    = "http://www.ressourcenmangel.de"
  s.summary     = "REST-Client to the TireFire API"
  s.description = "REST-Client to the TireFire API"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]

  s.add_dependency "rails", ">= 4.0.4"
  s.add_dependency "httparty"
  s.add_dependency "hashie"

  s.add_development_dependency "sqlite3"
  s.add_development_dependency "rspec-rails"
  s.add_development_dependency "capybara"
end
