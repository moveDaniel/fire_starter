module FireStarter
  class TagCategory < ApiObject

    def self.all
      response = HTTParty.get(FireStarter.config.tag_categories_url)
      if response.code == 200
        JSON.parse(response.body)['tag_categories'].map do |attributes|
          new attributes
        end
      else
        []
      end
    end

    def self.find(id)
      response = HTTParty.get(FireStarter.config.tag_category_url(id))
      if response.code == 200
        new JSON.parse(response.body)['tag_category']
      end
    end

    def tags
      self['tags'].map do |tag_attributes|
        Tag.new tag_attributes
      end
    end

    def save
      if self.persisted?
        response = HTTParty.put(FireStarter.config.tag_category_url(id), body: {tag_category: self})
      else
        response = HTTParty.post(FireStarter.config.tag_categories_url, body: {tag_category: self})
      end

      if response.code >= 400
        apply_errors JSON.parse(response.body)['meta']['errors']['tag_category']
        false
      else
        true
      end
    end

    def destroy
      response = HTTParty.delete(FireStarter.config.tag_category_url(self.id))
      if response.code == 200
        @destroyed = true
      end
      self
    end
  end
end
