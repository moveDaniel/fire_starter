module FireStarter
  class SearchQuery
    attr_reader :total, :total_pages, :per_page, :current_page, :documents
    delegate :first, :last, :each, to: :documents
    
    def initialize(response)
      @total = response['meta']['total']
      @total_pages = response['meta']['total_pages']
      @per_page = response['meta']['per_page']
      @current_page = response['meta']['current_page']

      @documents = response['documents'].map do |document|
         document['doc_type'].constantize.new(document['extra'])
      end
    end

    def each(&block)
      documents.each(&block)
    end
  end
end