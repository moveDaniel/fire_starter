module FireStarter
  class DidYouMean
    attr_reader :text, :highlighted, :score

    def initialize(did_you_mean)
      @text = did_you_mean['text']
      @highlighted = did_you_mean['highlighted']
      @score = did_you_mean['score']
    end
  end
end
