require 'active_support/configurable'

module FireStarter
  # Configures global settings for FireStarter
  #   FireStarter.configure do |config|
  #     config.host = localhost
  #     congig.port = 80
  #     config.tenant = nil
  #   end
  def self.configure(&block)
    yield @config ||= FireStarter::Configuration.new
  end

  # Global settings for FireStarter
  def self.config
    @config
  end

  
  class Configuration #:nodoc:
    include ActiveSupport::Configurable
    config_accessor :host
    config_accessor :port
    config_accessor :tenant

    def param_name
      config.param_name.respond_to?(:call) ? config.param_name.call : config.param_name
    end

    # define param_name writer (copied from AS::Configurable)
    writer, line = 'def param_name=(value); config.param_name = value; end', __LINE__
    singleton_class.class_eval writer, __FILE__, line
    class_eval writer, __FILE__, line

    def api_url
      "http://#{config.host}:#{config.port}/api/v1"
    end

    def status_url
      api_url + '/status'
    end    

    def documents_url
      [api_url, config.tenant, :documents].join('/')
    end

    def document_url(id)
      [api_url, config.tenant, :documents, id].join('/')
    end

    def delete_by_url
      [documents_url, :_delete_by].join('/')
    end

    def search_url
      [documents_url, :search].join('/')
    end

    def tag_categories_url
      [api_url, config.tenant, :tag_categories].join('/')
    end

    def tag_category_url(tag_category_id)
      [tag_categories_url, tag_category_id].join('/')
    end

    def tag_category_tags_url(tag_category_id)
      [tag_categories_url, tag_category_id, :tags].join('/')
    end

    def tag_category_tag_url(tag_category_id, tag_id)
      [tag_category_tags_url(tag_category_id), tag_id].join('/')
    end
  end

  
  configure do |config|
    yaml_path = 'config/tire_fire.yml'
    if File.exists?(yaml_path)
      yaml = YAML.load_file(yaml_path)
      yaml_config = (yaml[Rails.env] || {}).symbolize_keys
    else
      yaml_config = {}
    end

    config.host = yaml_config[:host] || 'localhost'
    config.port = yaml_config[:port] || 80
    config.tenant = yaml_config[:tenant]
  end
end
