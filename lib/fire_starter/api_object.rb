module FireStarter
  class ApiObject < Hashie::Mash
    extend ActiveModel::Translation
    include ActiveModel::Conversion

    attr_reader :errors
  
    def initialize(source_hash = nil, default = nil, &block)
      @errors = ActiveModel::Errors.new(self)
      @destroyed = false
      super
    end
    
    def to_s
      self.name
    end

    def update(attributes)
      self.merge!(attributes)
      save
    end

    def apply_errors(errors = {})
      errors.each do |attribute, messages|
        messages.each do |message|
          self.errors.add(attribute, message.to_sym)
        end
      end
    end

    def persisted?
      self.id.present?
    end

    def destroyed?
      @destroyed
    end
  
    def self.i18n_scope
      :firestarter
    end

    def self.create(attributes)
      object = new attributes
      object.save
      object
    end
  end
end