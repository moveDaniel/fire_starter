require 'fire_starter/did_you_mean'
require 'fire_starter/facet'

module FireStarter
  class Result
    attr_reader :total_count, :total_pages, :limit_value, :per_page, :current_page, :documents, :did_you_mean, :facets
    delegate(
      :count,
      :size,
      :first,
      :last,
      :each,
      :map,
      :any?,
      :blank?,
      :include?,
      to: :documents
    )

    def initialize(response)
      @total_count = response['meta']['total']
      @total_pages = response['meta']['total_pages']
      @limit_value = @per_page = response['meta']['per_page']
      @current_page = response['meta']['current_page']

      @documents = response['documents'].map do |document|
        document_class = document.delete('doc_type').constantize
        document_class.new(
          document['extra'].extract!(*document_class.attribute_names),
          highlight: document['highlight'],
          sort: document['sort'],
          distance: response['meta']['location'].present? ? document['sort'].last : nil
        )
      end

      @did_you_mean = response['meta']['did_you_mean'].to_a.map do |value|
        FireStarter::DidYouMean.new(value)
      end

      @facets = Hash[response['meta']['facets'].map{ |key, value|
        [key, FireStarter::Facet.new(value)]
      }]
    end

    def to_ary
      documents
    end

    def next_page
      current_page + 1 unless last_page?
    end

    def prev_page 
      current_page - 1 unless first_page?
    end

    def last_page?
      current_page == @total_pages
    end

    def first_page?
      current_page == 1
    end

    def out_of_range?
      current_page > @total_pages
    end
  end
end