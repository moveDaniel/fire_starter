module FireStarter
  class Tag < ApiObject

    attr_writer :tag_category

    # Initialize undefined attributes some values.
    # 
    def initialize(source_hash = nil, default = nil, &block)
      super
      self.id ||= nil
      self.name ||= nil
      self.aliases ||= []
    end

    def self.find(tag_category_id, id)
      response = HTTParty.get(FireStarter.config.tag_category_tag_url(tag_category_id, id))
      if response.code == 200
        new JSON.parse(response.body)['tag']
      end
    end

    def save
      if self.persisted?
        response = HTTParty.put(FireStarter.config.tag_category_tag_url(self.tag_category_id, self.id), body: {tag: self})
      else
        response = HTTParty.post(FireStarter.config.tag_category_tags_url(self.tag_category_id), body: {tag: self})
      end

      if response.code >= 400
        apply_errors JSON.parse(response.body)['meta']['errors']['tag']
        false
      else
        true
      end
    end

    def destroy
      response = HTTParty.delete(FireStarter.config.tag_category_tag_url(self.tag_category_id, self.id))
      if response.code == 200
        @destroyed = true
      end      
      self
    end
  end
end
