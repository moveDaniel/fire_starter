module FireStarter
  class Facet
    attr_reader :terms
    delegate :each, to: :terms

    def initialize(facet)
      @terms = Hash[facet['terms'].map { |hash|
        [hash['term'], hash['count'].to_i]
      }]
    end

    def has_term?(term)
      @terms.has_key? term
    end

    def count(term)
      @terms[term].to_i
    end
  end
end