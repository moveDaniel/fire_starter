require 'httparty'
require 'fire_starter/result'

module FireStarter
  module Document
    extend ActiveSupport::Concern

    included do

      class_attribute :custom_facets

      attr_reader :highlight, :sort, :distance

      # Initialize object and set additional search related
      # attributes +highlight+, +sort+, and +distance+
      #
      def initialize(attributes = nil, options = {})
        @highlight = options.delete(:highlight)
        @sort = options.delete(:sort)
        @distance = options.delete(:distance)
        super
      end

      # Returns a hash of the document's attributes
      # that should be indexed.
      #
      # Implement this method in your models.
      # @see {TireFire API documentation}[https://gitlab.rsm-support.de/rails/tirefire/blob/develop/api_doc.md#document-model-parameters-documents-api-document-model-parameters]
      #
      def to_document; {}; end

      # Adds the document to the index
      #
      def add_to_tire_fire_index
        self.class.add_to_tire_fire_index self.to_document.merge({
          source_id: self.source_id,
          doc_type: self.class.name,
          self.as_json
        })
      end

      # Removes the document from the index
      #
      def remove_from_tire_fire_index
        self.class.remove_from_tire_fire_index self.source_id
      end

      # Test whether the document has already been
      # added to the index.
      #
      def exists_in_tire_fire_index?
        self.class.exists_in_tire_fire_index? self.source_id
      end

      # Define custom facets
      #
      def self.facets_for(*fields)
        self.custom_facets = fields
      end

      # The document's id prefixed by its class name
      #
      def source_id
        "#{self.class.name.underscore}_#{self.id}"
      end
    end

    module ClassMethods
      # Returns the the API's status response
      #
      def api_status
        HTTParty.get(FireStarter.config.status_url)
        JSON.parse response.body
      end

      # Search for documents
      #
      def search(q=nil, options={})
        params = {q: q.blank? ? '*' : q}
        params = {
          q: (q.present? ? q : '*'),
          facets: self.custom_facets,
          scope: options.delete(:scope) || {},
          page: options.delete(:page) || 1,
          per_page: options.delete(:per_page) || 10
        }

        params.merge! options

        params[:scope][:doc_type] = self.name

        response = HTTParty.get(FireStarter.config.search_url, body: params)
        if response.code == 200
          FireStarter::Result.new(JSON.parse(response.body))
        end
      end

      # Adds a document to the index
      #
      def add_to_tire_fire_index(document)
        response = HTTParty.post(FireStarter.config.documents_url, body: {document: document})
        JSON.parse response.body
      end

      # Removes a document from the index
      #
      def remove_from_tire_fire_index(source_id)
        response = HTTParty.delete(FireStarter.config.delete_by_url, body: {doc_type: self.name, source_id: source_id})
        JSON.parse response.body
      end

      # Test whether a document exists in the index.
      #
      def exists_in_tire_fire_index?(source_id)
        search('*', source_id: source_id).total_count === 1
      end
    end
  end
end