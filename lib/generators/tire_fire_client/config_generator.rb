module FireStarter
  module Generators
    class ConfigGenerator < Rails::Generators::Base
      source_root File.expand_path(File.join(File.dirname(__FILE__), 'templates'))

      desc "Creates client configuration in config/tire_fire.yml"
      def copy_config_files
        template 'tire_fire.yml', 'config/tire_fire.yml'
      end
    end
  end
end
