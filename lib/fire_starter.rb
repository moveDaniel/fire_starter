require 'hashie'
require 'fire_starter/version'
require 'fire_starter/configuration'
require 'fire_starter/document'
require 'fire_starter/api_object'
require 'fire_starter/tag_category'
require 'fire_starter/tag'

module FireStarter

  # Returns the document found with the given
  # +source_id+ or +id+ as a +Hashie::Mash+
  def self.document(id)
    response = HTTParty.get self.config.document_url(id)
    if response.code == 200
      Hashie::Mash.new JSON.parse(response.body)['document']
    end
  end
end
